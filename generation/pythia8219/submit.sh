#!/bin/bash

gen="ckkw"

mkdir $PWD/output

for dsid in `seq 1 12`;
do
    
    echo "Sumbitting jobs for slice $dsid"
    echo ${dsid}

    input=/global/project/projectdirs/atlas/jennetd/multijetsMG/mc-multijets/generation/cms_${gen}/output/LHE/${dsid}

    files=`ls $input`

    i=1
    for f in $files;
    do

        qsub -l cvmfs=1 -l h_vmem=32G run.sh ${gen} ${dsid} ${i} $f
#	echo $f
	i=$(($i+1))

    done
done

