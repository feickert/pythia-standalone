#!/bin/bash
gen=$1
slice=$2
i=$3
input=$4

name=multijet_cms_${gen}_${i}

gen1=$gen
ckkw="ckkw"
if [ "$gen1" == "$ckkw" ]
then 
    gen1="ckkwl"
fi

script=main89${gen1}-lhapdf6_batch.cmnd

pythia_ver=pythia8.219

here=$PWD
outputdir=${here}/output
mkdir ${outputdir}

# Create running directory
rundir=/project/projectdirs/atlas/jennetd/batchjobs/running_directory_${gen}_slice${slice}_${i}
rm -rf ${rundir}
mkdir ${rundir}
cd ${rundir}

# Copy necessary files
cp ${here}/../${pythia_ver}.tar.gz .
tar -zxvf ${pythia_ver}.tar.gz
cp ${here}/../pythia8/examples/${script} ${pythia_ver}/examples

cp ${here}/../Generators.tar.gz .
tar -zxvf Generators.tar.gz
cp ${here}/../WorkArea.tar.gz .
tar -zxvf WorkArea.tar.gz

inputpath=/global/project/projectdirs/atlas/jennetd/multijetsMG/mc-multijets/generation/cms_${gen}/output/LHE/${slice}/${input}
echo ${inputpath}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

asetup --testarea=$PWD 19.2.5.5

#cd ${pythia_ver}
#make clean; make

# GET HEPMC
cd ${pythia_ver}/examples
cp ${inputpath} ./events-${gen}.lhe.gz
echo ${inputpath}
#make main89
./main89 ${script} OUT.${name}.hepmc >> pythia.txt

mkdir ${outputdir}/HEPMC
mkdir ${outputdir}/HEPMC/${slice}
cp OUT.${name}.hepmc ${outputdir}/HEPMC/${slice}/

mkdir ${outputdir}/pythia_out
mkdir ${outputdir}/pythia_out/${slice}
cp pythia.txt ${outputdir}/pythia_out/${slice}/pythia.${name}.txt

#GET EVNT
#cd ${rundir}/Generators/TruthIO/cmt
#cmt find_packages
#cmt compile

#cd ${rundir}/WorkArea/cmt
#cmt find_packages
#cmt compile

cd ${rundir}/WorkArea/ascii
ln -s ${rundir}/${pythia_ver}/examples/OUT.${name}.hepmc ./HEPevents.dat
ln -s ${rundir}/${pythia_ver}/examples/OUT.${name}.hepmc ./HEPevents.dat.1.events

Generate_tf.py --jobConfig=MC15.1.HepMCAscii_A_A_A.py --ecmEnergy=13000.0 --runNumber=1  --outputEVNTFile=mc.event.pool.root --inputGeneratorFile=HEPevents.dat

mkdir ${outputdir}/EVNT
mkdir ${outputdir}/EVNT/${slice}
cp mc.event.pool.root ${outputdir}/EVNT/${slice}/OUT.${name}.EVNT.pool.root

mkdir ${outputdir}/log
mkdir ${outputdir}/log/${slice}
cp log.generate ${outputdir}/log/${slice}/log${name}.dat

# DERIVATION
mkdir derivation
cd derivation
mv ../mc.event.pool.root .

asetup 20.1.8.3,AtlasDerivation,gcc48,here

Reco_tf.py --inputEVNTFile mc.event.pool.root --outputDAODFile OUT.pool.root --reductionConf TRUTH1

mkdir ${outputdir}/truthXAOD/
mkdir ${outputdir}/truthXAOD/${slice}
cp DAOD_TRUTH1.OUT.pool.root ${outputdir}/truthXAOD/${slice}/OUT.${name}.DAOD_TRUTH1.pool.root

cd ${here}
rm -rf ${rundir}

